package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmissionProfilesPage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public EmissionProfilesPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void VerifyEmissionProfile(String profile) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-Alliance_Access_Entry-frame")));
		driver.switchTo().frame("gwt-debug-Alliance_Access_Entry-frame");
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfElementLocated(
				By.id("gwt-debug--accessAdmin-configuration-emissionProfile-search-criteria-fieldName"))))
				.sendKeys(profile);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfElementLocated(
				By.id("gwt-debug--accessAdmin-configuration-emissionProfile-search-criteria-actionSearch")))).click();
		Thread.sleep(1000);
		String status = wait
				.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("td.GAJUMYXDPPB:nth-child(12) > div:nth-child(1)")))
				.getText();
		String sessionStatus = wait
				.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("td.GAJUMYXDPPB:nth-child(13) > div:nth-child(1)")))
				.getText();

		System.out.println("Status of Emission Profile " + profile + " is : " + status);
		System.out.println("Session status of Emission Profile " + profile + "  is : " + sessionStatus);

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("gwt-debug--accessAdmin-configuration-emissionProfile-search-criteria-actionClear"))).click();
		Thread.sleep(2000);
		driver.findElement(By.id("gwt-debug--accessAdmin-configuration-emissionProfile-search-criteria-actionSearch"))
				.click();
		driver.switchTo().defaultContent();
	}

}
