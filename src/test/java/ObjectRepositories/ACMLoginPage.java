package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ACMLoginPage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public ACMLoginPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "div.auth_HL:nth-child(2)")
	WebElement loginoption;
	@FindBy(id = "gwt-debug-platform_login-username")
	WebElement username;
	@FindBy(id = "gwt-debug-platform_login-password")
	WebElement password;
	@FindBy(id = "gwt-debug-platform_login-logon")
	WebElement login;

	public WebElement LoginOption() {
		wait.until(ExpectedConditions.visibilityOf(loginoption));
		return loginoption;
	}

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public void LoginInstance() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-platform_login-instances"))).click();
		Thread.sleep(500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".auth_FDC > div:nth-child(1)")))
				.click();

	}

	public WebElement Login() throws InterruptedException {
		Thread.sleep(1000);
		return login;
	}

}
