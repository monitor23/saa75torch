package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ACMHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public ACMHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "gwt-debug-desktop-applications-com.swift.Access.emissionProfiles")
	WebElement emissionProfiles;
	@FindBy(id = "gwt-debug-desktop-applications-com.swift.Access.receptionProfiles")
	WebElement receptionProfiles;
	@FindBy(id = "gwt-debug-desktop-applications-com.swift.Access.messagePartners")
	WebElement messagePartners;

	public void VerifyLogin() {
		String title = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".swp_KQ"))).getText();
		// System.out.println(title);
		if (title.equals("Alliance Access/Entry Configuration 7.5")) {
			System.out.println("Login Successful");
		} else {
			System.out.println("Login Failed");
		}
	}

	public WebElement EmissionProfiles() {
		wait.until(ExpectedConditions.visibilityOf(emissionProfiles));
		return emissionProfiles;
	}

	public WebElement ReceptionProfiles() {
		wait.until(ExpectedConditions.visibilityOf(receptionProfiles));
		return receptionProfiles;
	}

	public WebElement MessagePartners() {
		wait.until(ExpectedConditions.visibilityOf(messagePartners));
		return messagePartners;
	}

	public void Logout() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("#gwt-debug-desktop-topmenu-Logout > div:nth-child(2) > div:nth-child(1)"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-dialog-ask-0-ok"))).click();
		Thread.sleep(2000);
		driver.close();
	}

}
