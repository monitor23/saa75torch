package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.ACMHomePage;
import ObjectRepositories.ACMLoginPage;
import ObjectRepositories.ReceptionProfilesPage;

public class ReceptionProfiles {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://192.168.11.18:2443/swp/group/accessconfig/#");

	}

	@Test
	public void ReceptionProfileValidation() throws InterruptedException {

		ACMLoginPage alp = new ACMLoginPage(driver);
		alp.LoginOption().click();
		alp.WaitFunction();
		alp.Username().sendKeys("admin");
		alp.Password().sendKeys("Venus2009Venus2009+");
		alp.LoginInstance();
		alp.Login().click();
		ACMHomePage ahp = new ACMHomePage(driver);
		ahp.VerifyLogin();
		ahp.ReceptionProfiles().click();
		ReceptionProfilesPage rpp = new ReceptionProfilesPage(driver);
		rpp.VerifyReceptionProfile("FA_RT_RCV_torch");
		rpp.VerifyReceptionProfile("FA_SnF");
		ahp.Logout();

	}

}
